# Scripts that available #
To deploy all the application ( **landingpage** & **boxbilling** ) <br />
`docker-compose up -d`
# Step by step to deploy #
1. Clone or download this repository https://gitlab.com/yosephfernando/niagahostertest.git <br />
`git clone https://gitlab.com/yosephfernando/niagahostertest.git`
2. If you download the repository than you have to extract it to your local folder
3. If you clone the repository using git than go to the folder <br />
`cd niagahostertest`
4. In your terminal run the docker compose command <br />
`docker-compose up -d`
# NOTES #
Please make sure port **81** and port **8004** is free in your local machine. <br /><br />
The **BOXBILLING** crendential :<br />
http://**{your_local}**/bb-admin <br />
username: fernandoyoseph6@gmail.com <br />
password: test123
