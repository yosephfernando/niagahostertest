<?php
require __DIR__ . '/vendor/autoload.php';

use DI\Container;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Ysp\Helper;
// use Psr\Log\LoggerInterface;

$container = new Container();
AppFactory::setContainer($container);
$container->set('view', function() {
    // return Twig::create('public/views', ['cache' => 'public/cache']);
    return Twig::create('public/views');
});

$app = AppFactory::create();
$app->add(TwigMiddleware::createFromContainer($app));
$app->addRoutingMiddleware();

$customErrorHandler = function (
    ServerRequestInterface $request,
    Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails,
    ?LoggerInterface $logger = null
) use ($app) {
    // $logger->error($exception->getMessage());

    $payload = ['error' => $exception->getMessage()];

    $response = $app->getResponseFactory()->createResponse();
    $response->getBody()->write(
        json_encode($payload, JSON_UNESCAPED_UNICODE)
    );

    return $response;
};

$errorMiddleware = $app->addErrorMiddleware(true, true, true, $logger);
$errorMiddleware->setDefaultErrorHandler($customErrorHandler);

$app->get('/', function ($request, $response, $args) {
    $prices = Helper::getPrices();
    $phpModules = Helper::getModules(15);
    return $this->get('view')->render($response, 'content.html', [
        'title' => 'test',
        'price' => $prices,
        'phpModule' => $phpModules
    ]);
})->setName('profile');

$app->run();

