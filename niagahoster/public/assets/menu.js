const menu = document.getElementById('navbar');
const widthOutput = document.getElementsByTagName('body');

function reportWindowSize() {
  widthOutput.textContent = window.innerWidth;

  if(widthOutput.textContent < 767 || widthOutput.textContent < 991){
    menu.classList.add('collapse');
    menu.classList.add('hide');
  }else{
    menu.classList.remove('collapse');
    menu.classList.remove('hide');
  }
}

window.onresize = reportWindowSize;
window.onload = reportWindowSize;