<?php
namespace Ysp;

class Helper 
{
    public function getModules($limit){
        $phpModuleJson = json_decode(file_get_contents("public/data/phpModule.json"), true);
        $mapped = array_chunk($phpModuleJson['phpModules'], $limit);
        return $mapped;
    }

    public function getPrices(){
        $priceJson = json_decode(file_get_contents("public/data/price.json"), true);
        return $priceJson['prices'];
    }
}